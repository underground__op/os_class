#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <fcntl.h>
 


#define LSH_TOK_BUFSIZE 64
#define LSH_TOK_DELIM " \t\r\n\a"

#ifndef SHELL_H
#define SHELL_H

#define BUF_SIZE 200

#define TRUE 1
#define FALSE 0

#define READ 0
#define WRITE 1

#define PIPE '|'
#define RIGHT '>'
#define LEFT '<'
#define AMP '&'

#define TOK_BUFSIZE 128
#define TOK_DELIM " \t\r\n\a"



//not done path

void interactive(void);
char *line_read(void);
char **line_split(char *line);
int base(char **args);
int base2(char filename[100]);
int cd(char **args);
int help(char **args);
int exit_now(char **args);
int execute(char **args);
int proc_launch(char **args);
int invoke_pipe(char **args1, char **args2);
int redirect(char **args1, char **args2, int left);
pid_t Fork(void);
int detect_symbol_pos(char **args);
int detect_symbol(char **args) ;
int args_len(char **args);

//int execute2(char *args);
#endif

char *builtin_str[] = {
  "cd",
  "help",
  "exit"
};

int (*builtin_func[]) (char **) = {
  &cd,
  &help,
  &exit_now
};

// array of special characters
int symbols[] = {
    PIPE,
    LEFT,
    RIGHT
};

int main(int argc, char **argv)
{
 
  if(argc==2){
	  
	//  printf(argv[1]);
	 base2(argv[1]); 
  }else{ 
  
  interactive(); //intercative loop
  
  }
  

  return EXIT_SUCCESS;
}

char *line_read(void) //get line
{
  char *line = NULL;
  ssize_t bufsize = 0; 
  getline(&line, &bufsize, stdin); //allocates buffer
  return line;
}

void interactive(void)
{
  
  char **args;
  char *input;
  int command;

  while (command){
    printf("wish> ");
    input = line_read();  //read line 
    args = line_split(input); //split line 
    command = execute(args); //run command -var

    free(input); //emty read -var
    free(args); //dispose arguments -var
  } 
}

char **line_split(char *line)
{
  int bufsize = LSH_TOK_BUFSIZE, pos = 0;
  char **args = malloc(bufsize * sizeof(char*));
  char *arg;

  if (!args) {
    fprintf(stderr, " allocation error\n");
    exit(EXIT_FAILURE);
  }

  arg = strtok(line, LSH_TOK_DELIM);
  while (arg != NULL) {
    args[pos] = arg;
    pos++;

    if (pos >= bufsize) {
      bufsize += LSH_TOK_BUFSIZE;
      args = realloc(args, bufsize * sizeof(char*));
      if (!args) {
        fprintf(stderr, " allocation error\n");
        exit(EXIT_FAILURE);
      }
    }

    arg = strtok(NULL, LSH_TOK_DELIM);
  }
  args[pos] = NULL;
  return args;
}

int base(char **args)
{
  pid_t p_id, w_pid;
  int stat;

  p_id = fork(); //replicate to get child
  if (p_id == 0) { //this child
   
    if (execvp(args[0], args) == -1) { 
      perror("lsh");
    }
    exit(EXIT_FAILURE);
  } else if (p_id < 0) { //if fork had error
    
    perror("lsh");
  } else {
   
    do { //parent
      w_pid = waitpid(p_id, &stat, WUNTRACED); //wait for child
    } while (!WIFEXITED(stat) && !WIFSIGNALED(stat));
  }

  return 1;
}

// process launch
int proc_launch(char **args) {
    pid_t pid, wpid;
    int status;

    // background flag
    int bg = FALSE;

    // process name to run
    char proc_name[strlen(args[0])];
    memset(proc_name, '\0', sizeof(proc_name));
    
    // check if there is '&' indicating background process
    if (args[0][strlen(args[0]) - 1] == '&') {
        strncpy(proc_name, args[0], strlen(args[0]) - 1);
        bg = TRUE;
    } else {
        // if no & just copy args[0] into proc_name
        strcpy(proc_name, args[0]);
    }

    pid = Fork();
    if (pid == 0) {
        // child process
        if (execvp(proc_name, args) < 0) {
            // error exec-ing
            fprintf(stderr, "shell: command not found: %s\n", args[0]);
            exit(EXIT_FAILURE);
        }
        exit(-1);
    } else {
        // parent process
        // don't wait if it's a background process
        if (!bg) {
            do {
                wpid = waitpid(pid, &status, WUNTRACED);
            } while (!WIFEXITED(status) && !WIFSIGNALED(status));
        }
    }

    return 1;
}

int base2(char filename[100])
{
	//printf("Received Script. Opening %s", filename);
	FILE *fptr;
	char line[200];
	char **args;
	fptr = fopen(filename, "r");
	if (fptr == NULL)
	{
		printf("\nUnable to open file.");
		return 1;
	}
	else
	{
		//printf("\nFile Opened. Parsing. Parsed commands displayed first.");
		while(fgets(line, sizeof(line), fptr))
		{
			//printf("\n%s", line);
			args=line_split(line);
			execute(args);
		}
	}
	free(args);
	fclose(fptr);
	return 1;
}



int builtins() {
  return sizeof(builtin_str) / sizeof(char *);
}

int cd(char **args)
{
  if (args[1] == NULL) {
    fprintf(stderr, " expected argument to \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      perror("lsh");
    }
  }
  return 1;
}

int help(char **args)
{
  int i;
  
  printf("Type program names and arguments, and hit enter.\n");
  printf("The following are built in:\n");

  for (i = 0; i < builtins(); i++) {
    printf("  %s\n", builtin_str[i]);
  }

  printf("Use the man command for information on other programs.\n");
  return 1;
}

int exit_now(char **args)
{
	return 0;
}

int execute(char **args)
{
    if (args[0] == NULL) {
        // empty command
        return 1;
    }

    // detect the special character and its position
    int sym = detect_symbol(args);
    int sym_pos = detect_symbol_pos(args);

    // in the event there is no pipe or redirection
    if (sym < 0 || sym_pos < 0) {
        for (int i = 0; i < builtins(); i++) {
            if (strcmp(args[0], builtin_str[i]) == 0) {
                // this is a built in command
                return (*builtin_func[i])(args);
            }
        }
        return proc_launch(args);

    // there is pipe and redirection
    } else {
        // length of args array
        int len = args_len(args);
        // args to the left
        char *args_left[sym_pos + 1];
        // args to the right
        char *args_right[len - sym_pos];

        // copy arguments on the left side
        int i = 0;
        for (; i < sym_pos; i++)
            args_left[i] = args[i];
        args_left[i++] = NULL;

        // copy arguments on the right side
        int j = 0;
        for (; args[i] != NULL; i++, j++)
            args_right[j] = args[i];
        args_right[j] = NULL;

        // if the special character is a PIPE
        if (symbols[sym] == PIPE)
            return invoke_pipe(args_left, args_right);
        else if (symbols[sym] == LEFT)
            return redirect(args_left, args_right, TRUE);
        else
            return redirect(args_left, args_right, FALSE);
    }

    return 1;
}

/*{
  return 0;
}

int execute(char **args)
{
  int i;

  if (args[0] == NULL) {
    // An empty command was entered.
    return 1;
  }

  for (i = 0; i < builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }

  return proc_launch(args);
}*/


/*
int execute2(char *args)
{
  int i;

  if (args[0] == NULL) {
    // An empty command was entered.
    return 1;
  }

  for (i = 0; i < builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }

  return base2(args);
}*/

// detect '|', '<<', '>>', '<', '>' in the command
int detect_symbol(char **args) {
    /**
    * '|' return 0
    * '<' return 1
    * '>' return 2
    *
    * if there is no symbol or empty command then return -1
    */

    // command is empty
    if (args[0] == NULL)
        return -1;

    for (int i = 0; args[i] != NULL; i++) {
        if (args[i][0] == PIPE)
            return 0;
        if (args[i][0] == LEFT)
            return 1;
        if (args[i][0] == RIGHT)
            return 2;
    }

    return -1;
}

// like the previous method, but instead return the position of the special character
int detect_symbol_pos(char **args) {
    // if the command is empty
    if (args[0] == NULL)
        return -1;

    for (int i = 0; args[i] != NULL; i++) {
        if (args[i][0] == PIPE ||
            args[i][0] == LEFT ||
            args[i][0] == RIGHT)
            return i;
    }

    // return -1 if nothing found
    return -1;
}

pid_t Fork(void) {
    pid_t pid;

    if ((pid = fork()) < 0) {
        fprintf(stderr, "fork error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    return pid;
}


int redirect(char **args1, char **args2, int left) {
    pid_t pid;
    int status;
    int fd;
    char *filename;
    filename = args2[0];

    if (left) {
        // it means the file serves as the source of input
        if ((fd = open(filename, O_RDONLY, 0755)) == -1) {
            fprintf(stderr, "shell: no such file or directory: %s\n", strerror(errno));
            return EXIT_FAILURE;
        }

        dup2(fd, STDIN_FILENO);
    } else {
        // it means the file serves as the target of output
        if ((fd = open(filename, O_RDWR | O_CREAT | O_TRUNC, 0755)) == -1) {
            fprintf(stderr, "shell: error creating file: %s\n", strerror(errno));
            return EXIT_FAILURE;
        }

        dup2(fd, STDOUT_FILENO);
    }
    return 1;
}

int invoke_pipe(char **args1, char **args2) {
    pid_t pid1, pid2;
    int status1, status2;
    int fd[2];      // file descriptor

    // pipe the fd
    if (pipe(fd) == -1) {
        fprintf(stderr, "pipe error\n");
        return EXIT_FAILURE;
    }

    // check if args1 is builtin
    for (int i = 0; i < builtins(); i++) {
        if (strcmp(args1[0], builtin_str[i]) == 0) {
            // this is a built in command
            pid1 = Fork();

            if (pid1 == 0) {
                // child process, which is the exec'd program that will get input
                // from the built in command

                // close the WRITE interface of the fd
                close(fd[WRITE]);
                dup2(fd[READ], STDIN_FILENO);

                if (execvp(args2[0], args2) < 0) {
                    fprintf(stderr, "shell: command not found: %s\n", strerror(errno));
                    exit(EXIT_FAILURE);
                }
            } else {
                // parent process, which sends the input to the exec'd program

                // close READ interface of the fd
                close(fd[READ]);
                dup2(fd[WRITE], STDOUT_FILENO);

                (*builtin_func[i])(args1);
                waitpid(pid1, &status1, WUNTRACED);

                return 1;
            }
        }
    }

    // if args1 is not built in
    pid1 = Fork();
    if (pid1 == 0) {
        // child 1
        // send input to pipe
        close(fd[READ]);
        dup2(fd[WRITE], STDOUT_FILENO);
        return proc_launch(args1);
    } else {
        // parent branch
        pid2 = Fork();
        if (pid2 == 0) {
            // child 2
            close(fd[WRITE]);
            dup2(fd[READ], STDIN_FILENO);
            return proc_launch(args2);
        } else {
            // parent branch
            close(fd[WRITE]);
            close(fd[READ]);

            waitpid(pid1, &status1, WUNTRACED);
            waitpid(pid2, &status2, WUNTRACED);

            return 1;
        }
    }
}

extern char **environ;

int args_len(char **args) {
    int i = 0;
    for (i = 0; args[i] != NULL; i++) {}
    return i;
}
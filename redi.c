#include <stdio.h>
 #include <stdlib.h>
 #include <unistd.h>
 #include <string.h>
 #include <fcntl.h>
 #include <sys/wait.h>

  void redirection1(char value1[], char file1[]);
   void redirection2(char value1[],char value2[],char file1[]);
 int main(int argc, char *argv[]) {
/*
//how to proper;y define and concatinate strings 
char str1[12] = "./";
char str2[12] = "World";
char *str3 = strcat(str1,str2);
printf("%s",str3);
//how to define and concatinate strings
*/

redirection1("dir","p3.txt");

 redirection2("wc","redi.c","p5.txt");
 
 return 0;
 }

 void redirection1(char value1[], char file1[])
 {
	char str4[12] = "./";
	char *str5 =strcat(str4,file1);
	 
	int rc = fork();
 if (rc < 0) { // fork failed; exit
 fprintf(stderr, "fork failed\n");
 exit(1);
 } else if (rc == 0) { // child: redirect standard output to a file
 close(STDOUT_FILENO);
 //open("./p4.txt", O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);
 open(str5, O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);
 //strcat("./",p4.txt") -when collecting from user 

 // now exec "wc"...
 char *myargs[3]; //creates aaray
// myargs[0] = strdup("wc"); // program: "wc" (word count) //-command
// myargs[1] = strdup("redi.c"); // argument: file to count //-parameters
 myargs[0] = strdup(value1); //command with no file
// myargs[1] = strdup("");
myargs[1] = NULL;
 myargs[2] = NULL; // marks end of array
 execvp(myargs[0], myargs); // runs word count
 } else { // parent goes down this path (main)
 int rc_wait = wait(NULL);
 } 
	 
	 
 }
 
 void redirection2(char value1[],char value2[],char file1[])
 {
	char str4[12] = "./";
	char *str5 =strcat(str4,file1);
	 
	int rc = fork();
 if (rc < 0) { // fork failed; exit
 fprintf(stderr, "fork failed\n");
 exit(1);
 } else if (rc == 0) { // child: redirect standard output to a file
 close(STDOUT_FILENO);
 //open("./p4.txt", O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);
 open(str5, O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);
 //strcat("./",p4.txt") -when collecting from user 

 // now exec "wc"...
 char *myargs[3]; //creates aaray
// myargs[0] = strdup("wc"); // program: "wc" (word count) //-command
// myargs[1] = strdup("redi.c"); // argument: file to count //-parameters
 myargs[0] = strdup(value1); //command with  file
// myargs[1] = strdup("");
myargs[1] = strdup(value2); //file of command
 myargs[2] = NULL; // marks end of array
 execvp(myargs[0], myargs); // runs word count
 } else { // parent goes down this path (main)
 int rc_wait = wait(NULL);
 } 
	 
	 
 }
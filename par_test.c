#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	#include <sys/wait.h>

int fork_n_execute(const char *command)
{
int status;
pid_t pid;

pid = fork ();

if(pid == 0)
{
/* This is the child process */
system(command); // execute the command
// we call exit() when system() returns to complete child process
exit(EXIT_SUCCESS);
}

else if(pid < 0)
{
/* The fork failed */
printf("Failed to fork(): %s", command);
status = -1;
}

/* This is the parent process
* incase you want to do something 
* like wait for the child process to finish
*/	
/*
else
if(waitpid(pid, &status, 0) != pid)
status = -1;
*/ 
return status;
}

int main(int argc, char *argv[])
{
/*if(argc == 1)
{
printf("Usage:%s ... ", argv[0]);
return(-1);
}*/

int i;
printf("%s", argv[i]);
/* Loop through argv */
for(i = 1; i < argc; i++)
	
fork_n_execute(argv[i]);

return 0;
}